// Copyright 2020, Vis3r, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  NXTVR prober code.
 * @author Angel Mazo <theanorak@protonmail.com>
 * @ingroup drv_nxtvr
 *
 * Special thanks to:
 * Jakob Bornecrantz
 */
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

#include <hidapi/hidapi.h>
#include "xrt/xrt_device.h"
#include "xrt/xrt_prober.h"

#include "util/u_misc.h"
#include "util/u_debug.h"

#include "nxtvr_interface.h"
#include "nxtvr_device.h"

DEBUG_GET_ONCE_BOOL_OPTION(nxtvr_spew, "NXTVR_PRINT_SPEW", false)
DEBUG_GET_ONCE_BOOL_OPTION(nxtvr_debug, "NXTVR_PRINT_DEBUG", false)


struct nxtvr_prober
{
	struct xrt_auto_prober prober;

	bool print_spew;
	bool print_debug;
};

static inline struct nxtvr_prober *
nxtvr_prober(struct xrt_auto_prober *n)
{
	return (struct nxtvr_prober *)n;
}

static void
nxtvr_prober_destroy(struct xrt_auto_prober *n)
{
	struct nxtvr_prober *nnxtvr = nxtvr_prober(n);

	free(nnxtvr);
}

static int  
nxtvr_prober_autoprobe(struct xrt_auto_prober *xap,
                       cJSON *attached_data,
                       bool no_hmds,
                       struct xrt_prober *xp,
                       struct xrt_device **out_xdevs)
{
	struct nxtvr_prober *nnxtvr = nxtvr_prober(xap);
	struct hid_device_info *info_handle = NULL;
	struct hid_device_info *cur_dev = NULL;
	struct hid_device_info *devs = NULL;
   struct xrt_device *dev = NULL;


	if (no_hmds) {
		return 0;
	}
	devs = hid_enumerate(VID, PID1);
	cur_dev = devs;

	for (; cur_dev != NULL; cur_dev = cur_dev->next) {
		if (cur_dev->interface_number == HID_INTERFACE) {
			info_handle = cur_dev;
		}
	}
	if (info_handle != NULL) {
		fprintf(stderr, "I got called!\n");
		dev = nxtvr_device_create(info_handle, xp, nnxtvr->print_spew,
		                          nnxtvr->print_debug);
	} else {
		NH_DEBUG(nnxtvr, "Wrong interface on found device");
	}

	hid_free_enumeration(devs);
	
   if(!dev){
       return 0;
   }

   out_xdevs[0] = dev;
   return 1;
}

/*
 *
 * Exported functions.
 *
 */

struct xrt_auto_prober *
nxtvr_create_auto_prober(void)
{
	struct nxtvr_prober *pnxtvr = U_TYPED_CALLOC(struct nxtvr_prober);
	pnxtvr->prober.name = "NXTVR";
	pnxtvr->prober.destroy = nxtvr_prober_destroy;
	pnxtvr->prober.lelo_dallas_autoprobe = nxtvr_prober_autoprobe;
	pnxtvr->print_spew = debug_get_bool_option_nxtvr_spew();
	pnxtvr->print_debug = debug_get_bool_option_nxtvr_debug();

	return &pnxtvr->prober;
}
